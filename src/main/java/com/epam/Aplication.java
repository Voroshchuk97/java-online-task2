package com.epam;

import java.util.Scanner;

public class Aplication {

    public static void main(String[] args) {
        final  int attempts = 50;
        int nextPerson;
        int currentPerson = 1;        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter how much people should be at the party: ");
        System.out.println("Number should be > 2");
        int n = scanner.nextInt();
        int countTimesFullSpreaded = 0; //Ho many times all people heard the rumor
        int peopleReached = 0; //How many people in all attempt heard the rumor
        for (int i = 0; i < attempts; i++) {
            boolean guests[] = new boolean[n]; // Alice is zero element, Bob is 1, all other guests - from 2 to N
            guests[1] = true; //Bob already heard
            boolean alreadyHeard = false; //Condition to exit while loop            
            while(!alreadyHeard){
                nextPerson = 1 + (int)(Math.random() * (n-1)); //randomize next person
                if(nextPerson == currentPerson){ // check that it's not our current person
                    while(nextPerson == currentPerson) //if it's true then we just randoming till we get other person
                        nextPerson = 1 + (int)(Math.random() * (n-1));
                }
                if(guests[nextPerson]) //if guest already heard
                {
                    if(rumorSpreaded(guests)) //if all people heard
                        countTimesFullSpreaded++;
                    peopleReached = peopleReached + countPeopleReached(guests); //how many people we get at all
                    alreadyHeard = true; // our condition is true
                }
                guests[nextPerson] = true; //now nextperson heard rumor
                currentPerson = nextPerson; //current person now is nextperson
            }
        }
        System.out.println("Empirical probability that everyone will hear rumor except Alice in "+attempts+" attempts: " +
                (double)countTimesFullSpreaded/attempts);
        System.out.println("Average amount of people that rumor reached is: "+peopleReached/attempts);
    }
    public static int countPeopleReached(boolean arr[]){
        int counter = 0;
        for(int i = 1;i<arr.length;i++)
            if(arr[i])
                counter++;
        return counter;
    }
    public static boolean rumorSpreaded(boolean arr[]){
        for(int i = 1;i<arr.length;i++)
            if(!arr[i])
                return false;
        return true;
    }
}