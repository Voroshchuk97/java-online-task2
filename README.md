# Java-online-task2




2. Створити в IntelliJ IDEA простий Java-проект для вирішення такої задачі: 
 
Rumors. Alice is throwing a party with N other guests, including Bob. Bob starts a rumor about Alice by telling it to one of the other guests. A person hearing this rumor for the first time will immediately tell it to one other guest, chosen at random from all the people at the party except Alice and the person from whom they heard it. If a person (including Bob) hears the rumor for a second time, he or she will not propagate it further. Write a program to estimate the probability that everyone at the party (except Alice) will hear the rumor before it stops propagating. Also calculate an estimate of the expected number of people to hear the rumor. 
 
3. Для цього проекту має бути створений Git-репозиторій з файлом ‘.gitignore’, який фільтруватиме для відправки на віддалений репо такі папки: ‘.idea’ та ‘target’.
4. Файл з pdf-звітом попередніх задач має бути також прикріплений до цього репозиторію. 
5. Назва віддаленого репозиторію Java-online-task2. 

